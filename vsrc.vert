#version 330
uniform mat4 pMatrix,vMatrix,mMatrix;
layout (location = 0) in vec3 aPosition;
out float vid;

void main(void)
{
    gl_Position = pMatrix * vMatrix *mMatrix * vec4(aPosition ,1);
    gl_PointSize = 64.0;
    vid = float(gl_VertexID);
}
