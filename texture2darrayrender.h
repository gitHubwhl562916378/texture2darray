#ifndef TEXTURE2DARRAYRENDER_H
#define TEXTURE2DARRAYRENDER_H

#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QOpenGLExtraFunctions>
class Texture2DArrayRender
{
public:
    Texture2DArrayRender() = default;
    void initsize(int pointCount,QVector<QImage> &imgs,int width,int height);
    void render(QOpenGLExtraFunctions *f,QMatrix4x4 &pMatrix,QMatrix4x4 &vMatrix,QMatrix4x4 &mMatrix);

private:
    QOpenGLShaderProgram program_;
    QOpenGLTexture *texture_{nullptr};
    QOpenGLBuffer vbo_;
};

#endif // TEXTURE2DARRAYRENDER_H
