#include <QDir>
#include <QDebug>
#include "widget.h"

Widget::Widget(QWidget *parent)
    : QOpenGLWidget(parent)
{
}

Widget::~Widget()
{

}

void Widget::resizeGL(int w, int h)
{
    pMatrix_.setToIdentity();
    pMatrix_.perspective(45,float(w)/h,0.01f,100.0f);
}

void Widget::paintGL()
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();
    f->glClearColor(0.0f,0.0f,0.0f,1.0f);
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QMatrix4x4 vMatrix;
    vMatrix.lookAt(camera_,QVector3D(0.0,0.0,0.0),QVector3D(0.0,1.0,0.0));

    QMatrix4x4 mMatrix;
    render_.render(f,pMatrix_,vMatrix,mMatrix);
}

void Widget::initializeGL()
{
    QVector<QImage> imgs;
    QDir imgDir(".");
    foreach (const QFileInfo &fileInfor, imgDir.entryInfoList(QStringList() << "*.png",QDir::Files)) {
        imgs << QImage(fileInfor.absoluteFilePath());
    }
    render_.initsize(20,imgs,128,128);
    camera_.setX(0);
    camera_.setY(0);
    camera_.setZ(3);
}
