#include <random>
#include "texture2darrayrender.h"

void Texture2DArrayRender::initsize(int pointCount, QVector<QImage> &imgs, int width, int height)
{
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Vertex,"vsrc.vert");
    program_.addCacheableShaderFromSourceFile(QOpenGLShader::Fragment,"fsrc.frag");
    program_.link();

    texture_ = new QOpenGLTexture(QOpenGLTexture::Target2DArray);
    texture_->create();
    texture_->setSize(width,height);
    texture_->setLayers(imgs.count());
    texture_->setFormat(QOpenGLTexture::RGB8_UNorm);
    texture_->allocateStorage();
    for(int i = 0; i < imgs.count(); i++){
        QImage cvtImg(imgs.at(i).convertToFormat(QImage::Format_RGB888));
        texture_->setData(0,i,QOpenGLTexture::RGB,QOpenGLTexture::UInt8,cvtImg.constBits());
    }
    texture_->setMinMagFilters(QOpenGLTexture::Nearest,QOpenGLTexture::Nearest);
    texture_->setWrapMode(QOpenGLTexture::ClampToEdge);

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dis(-1.0,1.0);
    QVector<GLfloat> pointVec;
    for(int i = 0; i < pointCount * 3; i++){
        pointVec << dis(gen);
    }
    vbo_.create();
    vbo_.bind();
    vbo_.allocate(pointVec.data(),pointVec.count() * sizeof GLfloat);
}

void Texture2DArrayRender::render(QOpenGLExtraFunctions *f, QMatrix4x4 &pMatrix, QMatrix4x4 &vMatrix, QMatrix4x4 &mMatrix)
{
    f->glEnable(GL_DEPTH_TEST);
    f->glEnable(GL_BLEND);
    f->glEnable(GL_DEPTH_TEST);
    f->glEnable(GL_POINT_SPRITE);
    f->glEnable(GL_PROGRAM_POINT_SIZE);
    f->glEnable(GL_TEXTURE_2D_ARRAY); //默认是开启的

    program_.bind();
    vbo_.bind();
    f->glActiveTexture(GL_TEXTURE0 + 0);
    program_.setUniformValue("pMatrix",pMatrix);
    program_.setUniformValue("vMatrix",vMatrix);
    program_.setUniformValue("mMatrix",mMatrix);
    program_.setUniformValue("sTexture",0);

    program_.enableAttributeArray(0);
    program_.setAttributeBuffer(0,GL_FLOAT,0,3,3 * sizeof GLfloat);
    texture_->bind(0);
    f->glDrawArrays(GL_POINTS,0,vbo_.size() / (3 * sizeof (GLfloat)));

    program_.disableAttributeArray(0);
    texture_->release();
    vbo_.release();
    f->glDisable(GL_DEPTH_TEST);
    f->glDisable(GL_BLEND);
    f->glDisable(GL_DEPTH_TEST);
    f->glDisable(GL_POINT_SPRITE);
    f->glDisable(GL_PROGRAM_POINT_SIZE);
    f->glDisable(GL_TEXTURE_2D_ARRAY);
}
