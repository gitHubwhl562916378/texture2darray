#version 330
uniform sampler2DArray sTexture;
in float vid;
out vec4 fragColor;

void main(void)
{
    vec3 texCoord = vec3(gl_PointCoord.st,vid);
    fragColor = texture(sTexture,texCoord);
}
